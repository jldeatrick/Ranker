import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Ranker {

    private static JFrame f;
    private static JPanel p;
    private static JButton b;
    private static JButton one;
    private static JButton two;
    private static JLabel l;
    private static File file;
    private static ArrayList<String> rlist;
    private static ArrayList<String> plist;
    private static ArrayList<String> results;

    public static void main(String[] args) {
        f = new JFrame("List Ranker");
        f.setVisible(true);
        f.setSize(500,500);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        set();
    }

    private static void set(){

        p = new JPanel();
        p.setBackground(Color.WHITE);
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        b = new JButton("Choose a Text File");
        p.add(b);
        f.add(p);
        f.repaint();
        f.setVisible(true);
        b.addActionListener(e -> {
            try {
                initialize();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        });

    }

    private static void initialize() throws FileNotFoundException {
        JFileChooser chooser = new JFileChooser();

        int returnValue = chooser.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
        }
        rlist = new ArrayList<>();
        plist = new ArrayList<>();
        Scanner input = new Scanner(file);
        while (input.hasNext()){
            String nextline = input.nextLine();
            System.out.println(nextline);
            rlist.add(nextline);
            plist.add(nextline);
        }
        p.remove(b);
        JLabel label = new JLabel("The following entries were found:", JLabel.CENTER);
        p.add(label);
        JLabel label1 = new JLabel("\n", JLabel.CENTER);
        p.add(label1);
        Integer i = 1;
        for (String entry : rlist){
            JLabel e = new JLabel(i.toString() + ". " + entry);
            p.add(e);
            i++;
        }
        JLabel label2 = new JLabel("\n", JLabel.CENTER);
        p.add(label2);
        JLabel label3 = new JLabel("Is this correct?", JLabel.CENTER);
        p.add(label3);
        JButton yes = new JButton("Yes");
        p.add(yes);
        yes.addActionListener(e -> {
                tournament();
        });
        JButton no = new JButton("No");
        p.add(no);
        no.addActionListener(e -> {
            f.getContentPane().removeAll();
            set();
        });
        p.repaint();
        p.setVisible(true);
        f.setVisible(true);
        long seed = System.nanoTime();
        Collections.shuffle(rlist, new Random(seed));
        Collections.shuffle(plist, new Random(seed));
        for (String rname : rlist){
            for (String pname: plist){
            }
        }
    }

    private static void tournament(){

    }
}
